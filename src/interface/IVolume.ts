import IImageItem from "@/interface/IImageItem";

export default interface IVolume extends IImageItem {
    volume: number;
}
