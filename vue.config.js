module.exports = {
    publicPath: './', // Needed for Electron

    chainWebpack: config => {
        config
            .plugin('html')
            .tap(args => {
                args[0].title = 'Dockerun'
                return args
            })
    },
    pwa: {
        workboxOptions: {
            skipWaiting: true
        }
    }
}
